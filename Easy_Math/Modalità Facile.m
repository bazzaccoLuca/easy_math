//
//  Modalità Facile.m
//  Easy_Math
//
//  Created by luca bazzacco on 07/12/16.
//  Copyright © 2016 Baz&Akim. All rights reserved.
//

#import "Modalità Facile.h"

@interface Modalita__Facile ()

@end

@implementation Modalita__Facile
int h = 0;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSString *music = [[NSBundle mainBundle]           //musica sottofondo
                       pathForResource:@"Canzoni Per Bambini"
                       ofType:@"mp3"];
    audioPLayer = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:music] error:NULL];
    audioPLayer.delegate = self;
    audioPLayer.numberOfLoops=50;
    [audioPLayer play];
    [self reset];
    _indicatore.maximumValue = 100;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)indicatore:(id)sender {
    int indico = _indicatore.value;
    _vis.text = [NSString stringWithFormat:@"%d", indico];
}

- (IBAction)btnStart:(id)sender {
    
        int numeroInserito = _indicatore.value;
    
        _visualNumIns.text = [NSString stringWithFormat:@"%d", numeroInserito];
            _numero1.enabled = true;
            _numero2.enabled = true;
            _numero3.enabled = true;
            _numero4.enabled = true;
            _numero5.enabled = true;
            _numero6.enabled = true;
            _numero7.enabled = true;
            _numero8.enabled = true;
            _numero9.enabled = true;
            _numero10.enabled = true;
            _btnStart.enabled = false;   
}

- (IBAction)btn1Press:(id)sender {
    [self calcolo:1];
}
- (IBAction)btn2Press:(id)sender {
    [self calcolo:2];
}
- (IBAction)btn3Press:(id)sender {
    [self calcolo:3];
}
- (IBAction)btn4Press:(id)sender {
    [self calcolo:4];
}
- (IBAction)btn5Press:(id)sender {
    [self calcolo:5];
}
- (IBAction)btn6Press:(id)sender {
    [self calcolo:6];
}
- (IBAction)btn7Press:(id)sender {
    [self calcolo:7];
}
- (IBAction)btn8Press:(id)sender {
    [self calcolo:8];
}
- (IBAction)btn9Press:(id)sender {
    [self calcolo:9];
}
- (IBAction)btn10Press:(id)sender {
    [self calcolo:10];
}
- (IBAction)btnIndientro:(id)sender {
    [audioPLayer stop];
}
-(void)notificaVittoria
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"HAI VINTO!"
                          
                                                    message:@"Complimenti hai vinto!!"
                                                   delegate:nil
                                          cancelButtonTitle:@"NUOVA PARTITA"
                                          otherButtonTitles: nil];
    
    [alert show];
    
}
-(void)notificaScofitta
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"HAI PERSO!"
                          
                                                    message:@"Peccato hai perso, ritenta e sarai più fortunato!!"
                                                   delegate:nil
                                          cancelButtonTitle:@"NUOVA PARTITA"
                                          otherButtonTitles: nil];
    
    [alert show];
}
-(void)reset
{
    _vis.text = @"";
    _visualNumIns.text = @"";
    _numero1.enabled = false;
    _numero2.enabled = false;
    _numero3.enabled = false;
    _numero4.enabled = false;
    _numero5.enabled = false;
    _numero6.enabled = false;
    _numero7.enabled = false;
    _numero8.enabled = false;
    _numero9.enabled = false;
    _numero10.enabled = false;
    _btnStart.enabled = true;
}
-(void)calcolo:(int)numero
{
    int numeroCalcolato = [[_visualNumIns text] intValue];
    numeroCalcolato = numeroCalcolato - numero;
    _visualNumIns.text = [NSString stringWithFormat:@"%d", numeroCalcolato];
    if(numeroCalcolato == 0)
    {
        [self notificaVittoria];
        [self reset];
    }
    else if(numeroCalcolato < 0)
    {
        [self notificaScofitta];
        [self reset];
    }
}


@end

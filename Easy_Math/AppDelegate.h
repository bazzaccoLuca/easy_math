//
//  AppDelegate.h
//  Easy_Math
//
//  Created by luca bazzacco on 07/12/16.
//  Copyright © 2016 Baz&Akim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  ViewController.m
//  Easy_Math
//
//  Created by luca bazzacco on 07/12/16.
//  Copyright © 2016 Baz&Akim. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *music = [[NSBundle mainBundle]                       //musica sottofondo
                       pathForResource:@"Musica di sottofondo allegra"
                       ofType:@"mp3"];
    audioPLayer = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:music] error:NULL];
    audioPLayer.delegate = self;
    audioPLayer.numberOfLoops=50;
    [audioPLayer play];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnInfo:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Informazioni & Regole"
                          
                                                    message:@"Benvenuto in Easy Math...Applicazione ideata e sviluppata da Bazzacco & Jabroui. Le regole sono molto semplici, seleziona il numero che dovrai creare e divertiti premendo i numeri che troverai nel gioco. BUON DIVERTIMENTO!!"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    
    [alert show];
}

- (IBAction)btnFacile:(id)sender {
    [audioPLayer stop];
}

- (IBAction)btnDifficile:(id)sender {
    [audioPLayer stop];
}
@end

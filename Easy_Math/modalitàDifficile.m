//
//  modalitàDifficile.m
//  Easy_Math
//
//  Created by luca bazzacco on 07/12/16.
//  Copyright © 2016 Baz&Akim. All rights reserved.
//

#import "modalitàDifficile.h"


@interface modalita_Difficile ()

@end

@implementation modalita_Difficile


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _indicatoreNum.enabled = false;
    [self reset];
    [self disabilitaTasti];
    NSString *music = [[NSBundle mainBundle]           //musica sottofondo
                       pathForResource:@"Canzoni Per Bambini"
                       ofType:@"mp3"];
    audioPLayer = [[AVAudioPlayer alloc]initWithContentsOfURL:[NSURL fileURLWithPath:music] error:NULL];
    audioPLayer.delegate = self;
    audioPLayer.numberOfLoops=50;
    [audioPLayer play];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnStart:(id)sender {
    [_numeroInserito resignFirstResponder];
    int numeroInserito = _numeroInserito.value;
    int mosseMax = _indicatore.value;
    _numRagg.text = [NSString stringWithFormat:@"%d", numeroInserito];
    _numeroMosseMax.text = [NSString stringWithFormat:@"%d", mosseMax];
    [self visualizza];
    [self abilitaTasti];
    _btnStart.enabled = false;
    
}
- (IBAction)btn1Press:(id)sender {
    _btn1.enabled = false;
    [self calcolo:1];
}
- (IBAction)btn2Press:(id)sender {
    _btn2.enabled = false;
    [self calcolo:2];
}
- (IBAction)btn3Press:(id)sender {
    _btn3.enabled = false;
    [self calcolo:3];
}
- (IBAction)btn4Press:(id)sender {
    _btn4.enabled = false;
    [self calcolo:4];
}
- (IBAction)btn5Press:(id)sender {
    _btn5.enabled = false;
    [self calcolo:5];
}
- (IBAction)btn6Press:(id)sender {
    _btn6.enabled = false;
    [self calcolo:6];
}
- (IBAction)btn7Press:(id)sender {
    _btn7.enabled = false;
    [self calcolo:7];
}
- (IBAction)btn8Press:(id)sender {
    _btn8.enabled = false;
    [self calcolo:8];
}
- (IBAction)btn9Press:(id)sender {
    _btn9.enabled = false;
    [self calcolo:9];
}
- (IBAction)btn10Press:(id)sender {
    _btn10.enabled = false;
    [self calcolo:10];
}
- (IBAction)btnIndietro:(id)sender {
    [audioPLayer stop];
}

- (IBAction)indicatore:(id)sender {
    
    int indico = _indicatore.value;
    _vis.text = [NSString stringWithFormat:@"%d", indico];
    _indicatoreNum.enabled = true;
}

- (IBAction)indicatoreNum:(id)sender {
    if ([_vis.text  isEqual: @"2"])
    {
        _indicatoreNum.minimumValue = 1;
        _indicatoreNum.maximumValue = 19;
        int indicoNum = _indicatoreNum.value;
        
        _visNum.text = [NSString stringWithFormat:@"%d", indicoNum];
    }
    else if([_vis.text  isEqual: @"3"])
    {
        _indicatoreNum.minimumValue =19;
        _indicatoreNum.maximumValue = 27;
        int indicoNum = _indicatoreNum.value;
        
        _visNum.text = [NSString stringWithFormat:@"%d", indicoNum];
    }
    else if([_vis.text  isEqual: @"4"])
    {
        _indicatoreNum.minimumValue = 27;
        _indicatoreNum.maximumValue = 34;
        int indicoNum = _indicatoreNum.value;
        
        _visNum.text = [NSString stringWithFormat:@"%d", indicoNum];
    }
    else
    {
        _indicatoreNum.minimumValue = 34;
        _indicatoreNum.maximumValue = 40;
        int indicoNum = _indicatoreNum.value;
        
        _visNum.text = [NSString stringWithFormat:@"%d", indicoNum];
    }
}
- (void)messaggioPerso
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"HAI PERSO!"
                          
                                                    message:@"Peccato hai perso, ritenta e sarai più fortunato!!"
                                                   delegate:nil
                                          cancelButtonTitle:@"NUOVA PARTITA"
                                          otherButtonTitles: nil];
    [alert show];
    [self disabilitaTasti];
    [self reset];
    [self abilitaCount];
}
-(void)messaggioVinto
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"HAI VINTO!"
                          
                                                    message:@"Complimenti hai vinto!!"
                                                   delegate:nil
                                          cancelButtonTitle:@"NUOVA PARTITA"
                                          otherButtonTitles: nil];
    
    [alert show];
    [self disabilitaTasti];
    [self reset];
    [self abilitaCount];
}

-(void)calcolo: (int)numero
{
    
    int numeroCalcolato = [[_numeroAttuale text] intValue];
    numeroCalcolato = numeroCalcolato + numero;
    _numeroAttuale.text = [NSString stringWithFormat:@"%d", numeroCalcolato];
    int indico = _indicatore.value;
    int numeroMosse = [[_numeroMosse text] intValue];
    numeroMosse = numeroMosse + 1;
    _numeroMosse.text = [NSString stringWithFormat:@"%d", numeroMosse];
    int numeroInserito = _numeroInserito.value;
    if(numeroCalcolato == numeroInserito)
    {
        if(numeroMosse > indico)
        {
            [self messaggioPerso];
        }
        else
        {
            [self messaggioVinto];
        }
        
    }
    else if (numeroCalcolato < numeroInserito)
    {
        if(numeroMosse >= indico)
        {
            [self messaggioPerso];
        }
        
        
    }
    else
    {
        [self messaggioPerso];
    }
}

-(void)abilitaTasti
{
    _btn1.enabled = true;
    _btn2.enabled = true;
    _btn3.enabled = true;
    _btn4.enabled = true;
    _btn5.enabled = true;
    _btn6.enabled = true;
    _btn7.enabled = true;
    _btn8.enabled = true;
    _btn9.enabled = true;
    _btn10.enabled = true;
    _indicatore.enabled = false;
    _indicatoreNum.enabled = false;
}
-(void)abilitaCount
{
    _indicatore.enabled = true;
    _indicatoreNum.enabled = false;
}
-(void)disabilitaTasti
{
    _btn1.enabled = false;
    _btn2.enabled = false;
    _btn3.enabled = false;
    _btn4.enabled = false;
    _btn5.enabled = false;
    _btn6.enabled = false;
    _btn7.enabled = false;
    _btn8.enabled = false;
    _btn9.enabled = false;
    _btn10.enabled = false;
}

-(void)reset
{
    
    _numeroMosse.text = @"";
    _numeroAttuale.text = @"";
    _numRagg.text = @"";
    _Na.text = @"";
    _Nr.text = @"";
    _Mf.text = @"";
    _Mm.text = @"";
    _numeroMosseMax.text = @"";
    _btnStart.enabled = true;
    _vis.text = @"";
    _visNum.text =@"";
    _indicatore.value = 2;
    _indicatoreNum.value = 1;
    
}

-(void)visualizza
{
    _numeroAttuale.text = @"0";
    _Na.text = @"Numero attuale:";
    _numeroMosse.text = @"0";
    _Nr.text = @"Numero da creare:";
    _Mf.text = @"Mosse fatte:";
    _Mm.text = @"Mosse massime:";
    _vis.text = @"";
    _visNum.text = @"";
}
@end

//
//  Modalità Facile.h
//  Easy_Math
//
//  Created by luca bazzacco on 07/12/16.
//  Copyright © 2016 Baz&Akim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>     //framework che permette la riproduzione delle canzoni di sottofondo

@interface Modalita__Facile : UIViewController < UITextFieldDelegate, AVAudioPlayerDelegate>
{
    AVAudioPlayer * audioPLayer;
}

@property (weak, nonatomic) IBOutlet UILabel *lblErrore;
@property (weak, nonatomic) IBOutlet UILabel *visualNumIns;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UIButton *numero1;
@property (weak, nonatomic) IBOutlet UIButton *numero2;
@property (weak, nonatomic) IBOutlet UIButton *numero3;
@property (weak, nonatomic) IBOutlet UIButton *numero4;
@property (weak, nonatomic) IBOutlet UIButton *numero5;
@property (weak, nonatomic) IBOutlet UIButton *numero6;
@property (weak, nonatomic) IBOutlet UIButton *numero7;
@property (weak, nonatomic) IBOutlet UIButton *numero8;
@property (weak, nonatomic) IBOutlet UIButton *numero9;
@property (weak, nonatomic) IBOutlet UIButton *numero10;

@property (weak, nonatomic) IBOutlet UISlider *indicatore;
@property (weak, nonatomic) IBOutlet UILabel *vis;
- (IBAction)indicatore:(id)sender;

- (IBAction)btnStart:(id)sender;
- (IBAction)btn1Press:(id)sender;
- (IBAction)btn2Press:(id)sender;
- (IBAction)btn3Press:(id)sender;
- (IBAction)btn4Press:(id)sender;
- (IBAction)btn5Press:(id)sender;
- (IBAction)btn6Press:(id)sender;
- (IBAction)btn7Press:(id)sender;
- (IBAction)btn8Press:(id)sender;
- (IBAction)btn9Press:(id)sender;
- (IBAction)btn10Press:(id)sender;
- (IBAction)btnIndientro:(id)sender;


@end

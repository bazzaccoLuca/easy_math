//
//  modalitàDifficile.h
//  Easy_Math
//
//  Created by luca bazzacco on 07/12/16.
//  Copyright © 2016 Baz&Akim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>     //framework che permette la riproduzione delle canzoni di sottofondo

@interface modalita_Difficile : UIViewController <AVAudioPlayerDelegate, UITextFieldDelegate>

{
    AVAudioPlayer * audioPLayer;
}

@property (weak, nonatomic) IBOutlet UIButton *musica;
@property (weak, nonatomic) IBOutlet UILabel *vis;
@property (weak, nonatomic) IBOutlet UISlider *indicatoreNum;

@property (weak, nonatomic) IBOutlet UILabel *visNum;
@property (weak, nonatomic) IBOutlet UISlider *indicatore;
@property (weak, nonatomic) IBOutlet UILabel *Nr;
@property (weak, nonatomic) IBOutlet UILabel *Na;
@property (weak, nonatomic) IBOutlet UILabel *Mf;
@property (weak, nonatomic) IBOutlet UILabel *Mm;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
@property (weak, nonatomic) IBOutlet UILabel *numeroMosse;
@property (weak, nonatomic) IBOutlet UILabel *numeroMosseMax;
@property (weak, nonatomic) IBOutlet UISlider *numeroInserito;
@property (weak, nonatomic) IBOutlet UILabel *numeroAttuale;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIButton *btn7;
@property (weak, nonatomic) IBOutlet UIButton *btn8;
@property (weak, nonatomic) IBOutlet UIButton *btn9;
@property (weak, nonatomic) IBOutlet UIButton *btn10;
@property (weak, nonatomic) IBOutlet UILabel *numRagg;
- (IBAction)btnStart:(id)sender;
- (IBAction)musica:(id)sender;

- (IBAction)btn1Press:(id)sender;
- (IBAction)btn2Press:(id)sender;
- (IBAction)btn3Press:(id)sender;
- (IBAction)btn4Press:(id)sender;
- (IBAction)btn5Press:(id)sender;
- (IBAction)btn6Press:(id)sender;
- (IBAction)btn7Press:(id)sender;
- (IBAction)btn8Press:(id)sender;
- (IBAction)btn9Press:(id)sender;
- (IBAction)btn10Press:(id)sender;
- (IBAction)btnIndietro:(id)sender;
- (IBAction)indicatore:(id)sender;
- (IBAction)indicatoreNum:(id)sender;

@end

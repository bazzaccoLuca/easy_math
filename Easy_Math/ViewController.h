//
//  ViewController.h
//  Easy_Math
//
//  Created by luca bazzacco on 07/12/16.
//  Copyright © 2016 Baz&Akim. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>  //framework che permette la riproduzione delle canzoni di sottofondo

@interface ViewController : UIViewController <AVAudioPlayerDelegate>
{
    
    AVAudioPlayer * audioPLayer;
}
- (IBAction)btnInfo:(id)sender;
- (IBAction)btnFacile:(id)sender;
- (IBAction)btnDifficile:(id)sender;


@end

